package Simulator;

public class Event {
    private EventCallback callback;

    private boolean hasCalledBack;

    private EventType type;
    private Heading heading;

    private int startTime;
    private int endTime;

    private int runTime;

    public Event(Heading heading, EventType type, EventCallback callback) {
        this.callback = callback;
        this.heading = heading;
        this.type = type;

        if (type == EventType.Ventilation) {
            runTime = SimulationConstants.rngNextInt(12 * 60, 24 * 60);
        }
        else if (type == EventType.Hazard) {
            runTime = SimulationConstants.rngNextInt(1 * 60, 3 * 60);
        }
        else if (type == EventType.ServicesReq) {
            runTime = SimulationConstants.rngNextInt(5 * 60, 8 * 60);
        }
        else if (type == EventType.JumboDown) {
            runTime = SimulationConstants.rngNextInt(12 * 60, 24 * 60);
        }
        else if (type == EventType.StaffIllness) {
            runTime = SimulationConstants.rngNextInt(24 * 60, 5 * 24 * 60);
        }
        else if (type == EventType.Flooding) {
            runTime = SimulationConstants.rngNextInt(5 * 24 * 60, 7 * 24 * 60);
        }
        else if (type == EventType.MoveJumbo) {
            runTime = SimulationConstants.rngNextInt(40, 60);
        }
        else if (type == EventType.Rehab) {
            runTime = SimulationConstants.rngNextInt(17 * 60 + 30 - 2 * 60 + 20, 17 * 60 + 30 - 2 * 60 + 80);
        }

        startTime = EventManager.getTime();
        endTime = EventManager.getTime() + runTime;
    }

    public void update() {
        if (isActive() || hasCalledBack)
            return;

        callback.callback(heading, type);
        hasCalledBack = true;
    }

    public EventType getEventType() {
        return type;
    }

    public Heading getHeading() {
        return heading;
    }

    public int getStartTime() {
        return startTime;
    }

    public int getEndTime() {
        return endTime;
    }

    public int getRunTime() {
        return runTime;
    }

    public boolean isActive() {
        return EventManager.getTime() <= endTime;
    }
}
