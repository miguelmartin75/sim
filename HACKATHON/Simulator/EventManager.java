package Simulator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EventManager {
    private static int time;

    private static Map<Heading, List<Event>> events = new HashMap<Heading, List<Event>>();
    private static EventCallback eventCallback = null;

    public static double jumboDowntime = 0;

    public static void tick() {
        time += SimulationConstants.TICK_INCREMENT;

        // Update events
        for (Map.Entry<Heading, List<Event>> kvp : events.entrySet()) {
            if (kvp.getValue().size() == 0)
                continue;
            
            Event event = kvp.getValue().get(kvp.getValue().size() - 1);
            if (event.getEndTime() < time - 24 * 60) {
                kvp.getValue().remove(event);
                continue;
            }

            event.update();

            if (event.isActive() && event.getEventType().equals(EventType.MoveJumbo)) {
                jumboDowntime += SimulationConstants.TICK_INCREMENT;
            }

            if (!event.isActive())
                addRandomEvent(kvp.getKey());
        }
    }

    public static void add(Event event) {
        events.putIfAbsent(event.getHeading(), new ArrayList<Event>());
        events.get(event.getHeading()).add(event);
    }

    public static boolean eventActive(Heading heading) {
        return events.containsKey(heading) && events.get(heading).size() > 0 && events.get(heading).get(events.get(heading).size() - 1).isActive();
    }

    public static boolean isEventTypeActive(Heading heading, EventType type) {
        return events.containsKey(heading) && events.get(heading).size() > 0 && events.get(heading).get(events.get(heading).size() - 1).getEventType() == type;
    }

    public static void addRandomEvent(Heading heading) {
        if (eventCallback == null)
            return;

        final double[] eventProbabilities = {
                0.005 / 60,
                0.005 / 60,
                0.005 / 60,
                0.005 / 60,
                0.005 / 60,
                0.005 / 60
        };

        for (int i = 0; i < eventProbabilities.length; i++) {
            double rngValue = SimulationConstants.rng.nextDouble();

            if (rngValue <= eventProbabilities[i]) {
                add(new Event(heading, EventType.values()[i], eventCallback));
                heading.jumboDisabled = true;
                break;
            }
        }
    }

    public static void setCallback(EventCallback eventCallback) {
        EventManager.eventCallback = eventCallback;
    }

    public static List<Event> getEvents(Heading heading) {
        if (!events.containsKey(heading)) {
            return new ArrayList<Event>();
        }
        return events.get(heading);
    }

    public static int getTime() {
        return time;
    }
}
