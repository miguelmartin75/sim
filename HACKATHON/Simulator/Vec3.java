package Simulator;

public class Vec3 {
    public double x = 0;
    public double y = 0;
    public double z = 0;

    public Vec3() { }
    public Vec3(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
}
