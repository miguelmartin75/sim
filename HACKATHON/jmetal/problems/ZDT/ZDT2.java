//  ZDT2.java
//
//  Author:
//       Antonio J. Nebro <antonio@lcc.uma.es>
//       Juan J. Durillo <durillo@lcc.uma.es>
//
//  Copyright (c) 2011 Antonio J. Nebro, Juan J. Durillo
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

package jmetal.problems.ZDT;

import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.core.Variable;
import jmetal.encodings.solutionType.ArrayRealSolutionType;
import jmetal.encodings.solutionType.BinaryRealSolutionType;
import jmetal.encodings.solutionType.RealSolutionType;
import jmetal.util.JMException;
import jmetal.util.wrapper.XReal;

/**
 *  Class representing problem ZDT2
 */
public class ZDT2 extends Problem{
      
 /** 
  * Constructor.
  * Creates a default instance of  problem ZDT2 (30 decision variables)
  * @param solutionType The solution type must "Real", "BinaryReal, and "ArrayReal". 
  */
 private double bmaSlope = 0.0662588016;
    private double bmaIntercept = 39.13087458;
    private double bmaError = 0.04210560217;
    private double ahSlope = 0.888144069;
    private double ahIntercept = 13.43277347;
    private double ahError = 0.2172852289;
    private double jumSlope = 1.014603044;
    private double jumIntercept = 14.46464747;
    private double jumError = 0.00160641406;
  public ZDT2(String solutionType) throws ClassNotFoundException {
    this(solutionType, 3); // 30 variables by default
  } // ZDT2

  
 /** 
  * Constructor.
  * Creates a new ZDT2 problem instance.
  * @param numberOfVariables Number of variables
  * @param solutionType The solution type must "Real" or "BinaryReal".
  */
  public ZDT2(String solutionType, Integer numberOfVariables) {
    numberOfVariables_  = 3;
    numberOfObjectives_ =  1;
    numberOfConstraints_=  0;
    problemName_        = "Hackathon";

    upperLimit_ = new double[numberOfVariables_];
    lowerLimit_ = new double[numberOfVariables_];

    for (int var = 0; var < numberOfVariables_; var++){
      lowerLimit_[var] = 0.0;
      upperLimit_[var] = 5000;
    } //for

    if (solutionType.compareTo("BinaryReal") == 0)
    	solutionType_ = new BinaryRealSolutionType(this) ;
    else if (solutionType.compareTo("Real") == 0)
    	solutionType_ = new RealSolutionType(this) ;
    else if (solutionType.compareTo("ArrayReal") == 0)
    	solutionType_ = new ArrayRealSolutionType(this) ;
    else {
    	System.out.println("Error: solution type " + solutionType + " invalid") ;
    	System.exit(-1) ;
    }
  } //ZDT2
  
  /** 
  * Evaluates a solution 
  * @param solution The solution to evaluate
   * @throws JMException 
  */    
  public void evaluate(Solution solution) throws JMException {
		XReal x = new XReal(solution) ;
      Variable[] v = solution.getDecisionVariables();
      Double[] v2 = new Double[v.length];
      for (int i=0; i < v.length;i++) {
          v2[i] = v[i].getValue();
      }
    double [] fx = new double[numberOfObjectives_] ;
      fx[0] = bmaError*(bmaSlope*v2[0] + bmaIntercept)
              + ahError*(ahSlope*v2[1] + ahIntercept)
              + jumError*(jumSlope*v2[2] + jumIntercept);

    solution.setObjective(0,fx[0]);
      solution.setObjective(1,0);
    //solution.setObjective(1,fx[1]);
  } //evaluate
  
  /**
   * Returns the value of the ZDT2 function G.
   * @param  x Solution
   * @throws JMException 
   */  
  private double evalG(XReal x) throws JMException {
    double g = 0.0;        
    for (int i = 1; i < x.getNumberOfDecisionVariables();i++)
      g += x.getValue(i);
    double constant = (9.0 / (numberOfVariables_-1));
    g = constant * g;
    g = g + 1.0;
    return g;        
  } //evalG
    
  /**
   * Returns the value of the ZDT2 function H.
   * @param f First argument of the function H.
   * @param g Second argument of the function H.
   */
  public double evalH(double f, double g) {
    double h = 0.0;
    h = 1.0 - java.lang.Math.pow(f/g,2.0);
    return h;        
  } // evalH
} //ZDT2
