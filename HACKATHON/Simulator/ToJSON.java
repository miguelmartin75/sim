package Simulator;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.FileWriter;
import java.util.HashMap;
import java.util.Map;

public class ToJSON{

    private static FileWriter file;
	private static JSONObject main = new JSONObject();

	public static void writeToJson(Simulation sim, Double[] functionalValues, String filename) {

        ToJSON toJSON = new ToJSON(filename, functionalValues, sim.collisionSimulator.width,
                                  sim.collisionSimulator.height,
                                  sim.totalJumboCount, 0);

        try {
            JSONArray days = new JSONArray();
            for(int day = 0; day < sim.daysToRun; ++day) {
                JSONArray headings = new JSONArray();
                for (Heading heading : sim.collisionSimulator.getHeadings()) {
                    JSONObject object = toJSON.addItem(heading, day);
                    headings.put(object);
                }
                days.put(headings);
            }
            main.put("days", days);

            file.write(main.toString());
            file.flush();
            file.close();
        } catch(Exception e) {
            e.printStackTrace();
        }
	}

	public JSONObject addItem(Heading heading, int day){
		try {
			JSONObject h = new JSONObject();
            Vec2 endPosition = heading.getEndPoint(day);

            System.out.println("end point: " + endPosition);
			h.put("x1", heading.startPosition.x);
			h.put("y1", heading.startPosition.y);
            h.put("x2", endPosition.x);
            h.put("y2", endPosition.y);

            h.put("hasJumbo", heading.hasJumbo);

            Double distance = heading.advancedMetresPerDay.size() > day ? heading.advancedMetresPerDay.get(day) : null;
            h.put("isDrilling", distance != null && distance > 0);

            h.put("hasAirsupply", true);
            h.put("timeActive", heading.totalUptime);
            h.put("timeInactive", EventManager.getTime() - heading.totalUptime);

            h.put("advancedMetres", heading.getLength());

            distance = heading.rehabMetresPerDay.size() > day ? heading.rehabMetresPerDay.get(day) : null;
            h.put("noRehabed", distance != null && distance > 0);
            h.put("rehabMetres", heading.getTotalRehabLength());

            h.put("events", getEvents(heading, day));
            return h;

		}catch(Exception e) {
			e.printStackTrace();
		}
        return null;
	}

    public Map<String, Integer> getEvents(Heading heading, int day) {
        Map<String, Integer> events = new HashMap<String, Integer>();

        int currentDayStart = EventManager.getTime() - 24 * 60;
        for (Event event : EventManager.getEvents(heading)) {
            int runTime = event.getRunTime();
            if (event.getStartTime() <= currentDayStart && event.getEndTime() >= currentDayStart)
                 runTime = event.getEndTime() - currentDayStart;
            else if (event.getStartTime() <= EventManager.getTime() && event.getEndTime() >= EventManager.getTime())
                runTime = EventManager.getTime() - event.getStartTime();

            events.put(event.getEventType().toString(), runTime);
        }

        return events;
    }

	public ToJSON(String filename, Double[] functionalValues, int MineSizeX, int MineSizeY, int JumbosTotal, int ElectTotal){
		try {
			file = new FileWriter(filename);

			JSONObject obj = new JSONObject();
			obj.put("MineSizeX",MineSizeX);
			obj.put("MineSizeY",MineSizeY);
			obj.put("jumbos",JumbosTotal);
			obj.put("electricians",ElectTotal);
            obj.put("cumAdvancedDist", functionalValues[0]);
            obj.put("headingUtilisation", functionalValues[1]);
            obj.put("jumboUtilisation", functionalValues[2]);

			main.put("general", obj);

		} catch(Exception e) {
			e.printStackTrace();
		}
	}



}

