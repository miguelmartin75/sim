package Simulator;

import java.util.Random;

public class SimulationConstants {
    public static Random rng = new Random();

    public static boolean DEBUG = false;

    public static double JUMBO_DISTANCE_MEAN = 1.55; // for 2-3 hours
    public static double JUMBO_DISTANCE_MEAN_PER_MINUTE = 1.55 / (2*60 + 50);
    public static double JUMBO_DISTANCE_DEVIATION = 1.22 / 3;
    public static double INTERSECTION_RADIUS = 10;
    public static int JUMBO_TRANSFER_TIME = 50;
    public static int TICK_INCREMENT = 1;

    public static double SHAFT_MIN_RADIUS = 50;
    public static double SHAFT_MAX_RADIUS = 150;

    /**
     * Generates a random value in range [min, max)
     * @param min inclusive minimum value
     * @param max exclusive maximum value
     * @return the random value
     */
    public static int rngNextInt(int min, int max) {
        return rng.nextInt(max - min) + min;
    }

    /**
     * Adds a random variation to the value.
     * @param value the value to add variation to
     * @param minVariation the minimum variation
     * @param maxVariation the maximum variation
     * @return the varied value
     */
    public static int addRngVariation(int value, int minVariation, int maxVariation) {
        return value + (rng.nextBoolean() ? -1 : 1) * rngNextInt(minVariation, maxVariation);
    }
}
