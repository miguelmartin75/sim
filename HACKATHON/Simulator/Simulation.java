package Simulator;

import java.util.ArrayList;
import java.util.Comparator;

public class Simulation {

    public static void main(String[] args) {
        if (args.length < 6) {
            System.out.print("usage: <days to run> <amount of jumbos> <amount of headings> <width> <height>");
            System.exit(1);
        }

        Simulation sim = new Simulation(Integer.parseInt(args[0]), Integer.parseInt(args[1]),
                            Integer.parseInt(args[2]), Integer.parseInt(args[3]), Integer.parseInt(args[4]),
                            Integer.parseInt(args[5]),true);
        sim.run();
    }

    private int availableJumbos;
    public int daysToRun;
    public int totalJumboCount;

    CollisionSimulator collisionSimulator;

    boolean isWrite = true;
    public Simulation(int daysToRun, int amountOfJumbos, int amountOfHeadings, int width, int height, int type,boolean write) {
        this.daysToRun = daysToRun;
        this.availableJumbos = totalJumboCount = amountOfJumbos;
        isWrite = write;
        this.collisionSimulator = new CollisionSimulator(amountOfHeadings, width, height, 0, type);

        EventManager.setCallback(new EventCallback() {
            @Override
            public void callback(Heading heading, EventType type) {
                eventCallback(heading, type);
            }
        });
    }

    public void init() {
        int amount = Math.min(availableJumbos, collisionSimulator.getNumHeadings());
        for(int i = 0; i < amount; ++i) {
            collisionSimulator.getHeadings()[i].hasJumbo = true;
            availableJumbos--;
        }
    }

    public Double[] run() {
        init();
        printResults();

        for(int day = 0; day < daysToRun; ++day) {
            for(int minute = 0; minute < 24 * 60; minute += SimulationConstants.TICK_INCREMENT) {
                tick(day);
            }
        }

        printResults();

        Double[] functionalValues = new Double[3];

        for (int i = 0; i < functionalValues.length; i++)
            functionalValues[i] = 0.0;

        for (int i = 0; i < collisionSimulator.getHeadings().length; i++) {
            functionalValues[0] += collisionSimulator.getHeadings()[i].getLength();
            functionalValues[1] += collisionSimulator.getHeadings()[i].totalUptime;
        }

        functionalValues[1] = functionalValues[1] / (collisionSimulator.getHeadings().length + 1e-6) / (daysToRun * 24 * 60);
        if (SimulationConstants.DEBUG)
            System.out.println(EventManager.jumboDowntime+ " " + totalJumboCount+" " + 24* 60*daysToRun);
        functionalValues[2] = 1.0 - (EventManager.jumboDowntime / (totalJumboCount * daysToRun * 24 * 60));
        if (SimulationConstants.DEBUG)
            System.out.println(String.format("%f %f %f", functionalValues[0], functionalValues[1], functionalValues[2]));
        if (isWrite) ToJSON.writeToJson(this, functionalValues, "data.json");

        return functionalValues;
    }

    public void tick(int day) {
        EventManager.tick();

        Heading[] headings = collisionSimulator.getHeadings();
        for(int i = 0; i < headings.length; ++i) {
            final Heading heading = headings[i];

            if (EventManager.eventActive(heading))
                continue;

            if (collisionSimulator.doesCollide(heading)) {

                // This heading collides with another that has a jumbo
                if (!heading.hasJumbo) {
                    continue;
                }

                // This heading collides with at least one other heading that has a jumbo
                ArrayList<Heading> nearestWithoutJumbos = getClosestHeadingsWithoutJumbos(heading);

                if (nearestWithoutJumbos.size() == 0) {
                    // Don't remove the jumbo
                    heading.jumboDisabled = true;
                }
                else {
                    boolean hasMoved = false;

                    for (final Heading nearest : nearestWithoutJumbos) {
                        if (collisionSimulator.doesCollide(nearest)) {
                            continue;
                        }

                        heading.hasJumbo = false;
                        heading.jumboDisabled = true;

                        EventManager.add(new Event(heading, EventType.MoveJumbo, new EventCallback() {
                            @Override
                            public void callback(Heading heading, EventType type) {
                                nearest.jumboDisabled = false;
                                nearest.hasJumbo = true;
                            }
                        }));

                        hasMoved = true;
                        break;
                    }

                    if (!hasMoved) {
                        // Couldn't move jumbo to a free heading, just disable it
                        heading.jumboDisabled = true;
                    }
                }
            } else if (heading.hasJumbo) {
                // This heading has a jumbo but is not colliding, ensure it's enabled
                heading.jumboDisabled = false;
            } else if (availableJumbos > 0) {
                EventManager.add(new Event(heading, EventType.MoveJumbo, new EventCallback() {
                    @Override
                    public void callback(Heading heading, EventType type) {
                        heading.hasJumbo = true;
                        heading.jumboDisabled = false;
                    }
                }));

                availableJumbos--;

                if (SimulationConstants.DEBUG)
                    System.out.println("1:" + availableJumbos + " " + day);
            }

            if (heading.hasJumbo && !heading.jumboDisabled) {
                final double lengthDrilled = calculateLengthDrilled(heading);
                heading.addLengthForDay(day, lengthDrilled, new EventCallback() {
                    @Override
                    public void callback(Heading heading, EventType eventType) {
                        heading.hasJumbo = false;
                        heading.jumboDisabled = false;
                        heading.addRehabMetresForNextDay(heading.currentAdvancements);
                        heading.currentAdvancements = 0;
                    }
                });

                if (EventManager.isEventTypeActive(heading, EventType.Rehab))
                    availableJumbos++;

                    if (SimulationConstants.DEBUG)
                        System.out.println("2:" + availableJumbos + " " + day);
            }
        }
    }

    private double calculateLengthDrilled(Heading heading) {
        if(!heading.hasJumbo) { return 0; }

        double length = 0;
        for(int i = 0; i < SimulationConstants.TICK_INCREMENT; ++i) {
            length += SimulationConstants.JUMBO_DISTANCE_MEAN_PER_MINUTE + SimulationConstants.addRngVariation(0, 0, 10) / 100;
        }
        return length;
    }

    private ArrayList<Heading> getClosestHeadingsWithoutJumbos(final Heading current) {
        ArrayList<Heading> ret = new ArrayList<Heading>();

        for (int i = 0; i < collisionSimulator.getHeadings().length; i++) {
            if (!collisionSimulator.getHeadings()[i].hasJumbo)
                ret.add(collisionSimulator.getHeadings()[i]);
        }

        ret.sort(new Comparator<Heading>() {
            @Override
            public int compare(Heading o1, Heading o2) {
                return Double.compare(current.distanceToHeading(o2), current.distanceToHeading(o1));
            }
        });

        return ret;
    }

    public void eventCallback(Heading heading, EventType type) {
        if (heading.hasJumbo) {
            heading.jumboDisabled = false;
        }
    }

    private void printResults() {
        if (!SimulationConstants.DEBUG)
            return;

        int i = 1;
        for(Heading heading : collisionSimulator.getHeadings()) {
            System.out.println(String.format("Heading %d: %s, %s, %f", i, heading.startPosition, heading.endPosition, heading.getAngle()));
            for(int day = 0; day < daysToRun; ++day) {
                if(heading.advancedMetresPerDay.size() > day)
                    System.out.println("AdvMet: " + heading.advancedMetresPerDay.get(day));
                //System.out.println("Rehab: " + heading.rehabMetresPerDay.get(day));
            }
            ++i;
        }
    }
}
