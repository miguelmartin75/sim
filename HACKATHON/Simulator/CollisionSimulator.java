package Simulator;

import java.util.Random;

public class CollisionSimulator {
    public static final double INIT_DIST = 0.00001;
    public static final double EPS = 0.000001;

    private int numHeadings;

    public int width;
    public int height;
    public int depth;
    public double radius;
    public Vec2 center;

    private Heading[] headings;

    public CollisionSimulator(int numHeadings, int width, int height, int depth, int type) {
        this.numHeadings = numHeadings;
        this.width = width;
        this.height = height;
        this.depth = depth;

        headings = new Heading[numHeadings];
        radius = SimulationConstants.SHAFT_MIN_RADIUS + SimulationConstants.rng.nextDouble() * (SimulationConstants.SHAFT_MAX_RADIUS - SimulationConstants.SHAFT_MIN_RADIUS);

        center = getRandomMapPoint(width - 2 * radius, height - 2 * radius);
        center.x += radius;
        center.y += radius;

        Heading initialHeading = new Heading(getRandomMapPoint(width, height), getRandomMapPoint(width, height));

        for (int i = 0; i < numHeadings; i++) {
            Vec2 startPoint = null;
            Vec2 endPoint = null;

            switch (type) {
                case 0: {
                    // Random point on the initial heading
                    startPoint = initialHeading.getPositionOnLine(SimulationConstants.rng.nextDouble());
                    double angle1 = (Math.PI - initialHeading.getAngle()) + SimulationConstants.rng.nextDouble() * Math.PI;
                    double angle2 = (initialHeading.getAngle()) + SimulationConstants.rng.nextDouble() * Math.PI;
                    Vec2 endPoint1 = new Vec2(startPoint.x + INIT_DIST * Math.cos(angle1), startPoint.y + INIT_DIST * Math.sin(angle1));
                    Vec2 endPoint2 = new Vec2(startPoint.x + INIT_DIST * Math.cos(angle2), startPoint.y + INIT_DIST * Math.sin(angle2));

                    endPoint = SimulationConstants.rng.nextInt(2) == 0 ? endPoint1 : endPoint2;

                    break;
                }
                case 1: {
                    startPoint = getRandomMapPoint(width, height);
                    endPoint = getDirectionalPoint(startPoint, INIT_DIST);

                    break;
                }
                case 2: {
                    // Random points on heading
                    // Todo: This doesn't really work well since we don't know the distance from the start
                    startPoint = headings[SimulationConstants.rng.nextInt(i)].getPositionOnLine(SimulationConstants.rng.nextDouble());
                    endPoint = getDirectionalPoint(startPoint, INIT_DIST);

                    break;
                }
                case 3: {
                    startPoint = getDirectionalPoint(center, radius);
                    endPoint = getDirectionalPoint(startPoint, INIT_DIST);

                    break;
                }
            }

            headings[i] = new Heading(startPoint, endPoint);
        }
    }

    public int getNumHeadings() {
        return numHeadings;
    }

    public Heading[] getHeadings() {
        return headings;
    }

    /**
     * Determines if this heading collides with other headings
     * @return
     */
    public boolean doesCollide(Heading heading) {
        for (int i = 0; i < headings.length; i++) {
            Heading h = headings[i];
            if (h == heading)
                continue;
            if (heading.intersects(h) && !h.jumboDisabled)
                return true;
        }

        return false;
    }

    private Vec2 getDirectionalPoint(Vec2 offset, double distance) {
        double dist = SimulationConstants.rng.nextDouble() * distance;
        double direction = SimulationConstants.rng.nextDouble() * 2 * Math.PI;
        return new Vec2(offset.x + dist * Math.cos(direction), offset.y + dist * Math.sin(direction));
    }

    private Vec2 getRandomMapPoint(double width, double height) {
        return new Vec2(SimulationConstants.rng.nextDouble() * width, SimulationConstants.rng.nextDouble() * height);
    }
}

