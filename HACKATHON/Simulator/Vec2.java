package Simulator;

public class Vec2 {
    public double x = 0;
    public double y = 0;

    public Vec2() { }
    public Vec2(double x, double y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return String.format("(%f, %f)", x, y);
    }
}
