package Simulator;

public interface EventCallback {
    void callback(Heading heading, EventType eventType);
}
