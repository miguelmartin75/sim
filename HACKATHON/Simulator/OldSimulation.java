package Simulator; /**
 * Created by Jayden on 10/10/2015.
 */

import java.util.*;

public class OldSimulation {
    private int day;
    private int maxDays;
    private int headings;
    private List<Integer> eventCounter;
    private int jumbos;
    private List<Map<String,Double>> headingDetails;
    private List<Integer> active;
    List<Double> metresAdvancedPerHeading;

    public static void main(String[] args) throws Exception{
        OldSimulation simulation = new OldSimulation(100,30,25);

        simulation.getResults();
    }

    public OldSimulation(int days, int headings, int jumbos) {
        day = 0;
        this.headings = headings;
        this.jumbos = jumbos;
        this.maxDays = days;
        metresAdvancedPerHeading = new ArrayList<Double>();
        headingDetails = new ArrayList<Map<String, Double>>();
        eventCounter = new ArrayList<Integer>();
        active = new ArrayList<Integer>();
        setupSimulation();
        runSimulation();
    }
    private void setupSimulation() {
        for (int i=0; i < headings; i++) {
            metresAdvancedPerHeading.add(i, 0.0);
            active.add(i,0);
        }
        for (int i=0; i < maxDays; i++) {
            eventCounter.add(i, 0);
        }
    }
    private void runSimulation() {
        double stdev = 1.22/3;
        double mean = 1.55;
        for (day=0; day< maxDays ; day++) {
            for (int i=0; i < headings; i++) {
                metresAdvancedPerHeading.set(i, metresAdvancedPerHeading.get(i) + eventsAffectMetres((Math.random()*stdev*2)-stdev+mean,i)); //stdev*random + mean
            }
        }

    }

    private double eventsAffectMetres(double metresOfDay,int jumbo) {
        double newMetresOfDay = 0;
        double metresPerHour = metresOfDay/24;
        //go through all 24 hours
        int eventTime = active.get(jumbo);
        for (int i=0; i < 24; i++) {
            if ( eventTime == 0 ) {
                //ventalation/radio
                if ( Math.random() < 0.005/24 ) {
                    System.out.println("ventalation problem");
                    eventTime = (int)Math.round(Math.random()*4 + 2);
                    eventCounter.set(day,eventCounter.get(day)+1);
                }
                if ( Math.random() < 0.005/24) {
                    System.out.println("hazard");
                    eventTime = (int) Math.round(Math.random()*2 + 1);
                    eventCounter.set(day,eventCounter.get(day)+1);
                }
                if ( Math.random() < 0.01/24) {
                    System.out.println("services required");
                    eventTime = (int) Math.round(Math.random()*4 + 1);
                    eventCounter.set(day,eventCounter.get(day)+1);
                }
                if ( Math.random() < 0.005/24) {
                    System.out.println("jumbo down");
                    eventTime = (int) Math.round(Math.random()*24 + 1);
                    eventCounter.set(day,eventCounter.get(day)+1);
                }
                if ( Math.random() < 0.005/24) {
                    System.out.println("staff illness");
                    //check if extra staff
                    eventTime = (int) Math.round(24);
                    eventCounter.set(day,eventCounter.get(day)+1);
                }
                if ( Math.random() < 0.005/24) {
                    System.out.println("water discharge");
                    eventTime = (int) Math.round(Math.random()*3+5);
                    eventCounter.set(day,eventCounter.get(day)+1);
                }
                newMetresOfDay += metresPerHour;
            }else{
                eventTime -= 1;
            }

        }
        active.set(jumbo,eventTime);
        return newMetresOfDay;
    }

    public Map<Integer,Map<String,Double>> getResults() {
        double count = 0;
        for (int i=0; i < headings;i++) {
            count += metresAdvancedPerHeading.get(i);
            System.out.println("metres advanced " + metresAdvancedPerHeading.get(i));
        }
        System.out.println("total metres advanced" + count/maxDays);
        count = 0;
        for (int i=0; i < maxDays;i++) {
            count += eventCounter.get(i);
            System.out.println("events day "+i+": " + eventCounter.get(i));
        }
        System.out.println("total events" + count);
        return null;
    }

    // WAT
    public double getInvDist(double x) {
        double stdev = 0.4066666;
        double mean = 1.542;
        return mean + Math.sqrt( -2*stdev*stdev*Math.log(x*stdev*Math.sqrt(Math.PI*2)));
    }
}
