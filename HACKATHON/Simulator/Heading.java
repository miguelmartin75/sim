package Simulator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Heading {

    public Vec2 startPosition = new Vec2();
    public Vec2 endPosition = new Vec2();

    public List<Double> advancedMetresPerDay = new ArrayList<Double>();
    public List<Vec2> endPointsPerDay = new ArrayList<Vec2>();
    public List<Double> rehabMetresPerDay = new ArrayList<Double>();

    public boolean hasJumbo = false;
    public boolean jumboDisabled = true;

    public int currentDrillingTime = 0;
    public double currentAdvancements = 0;

    public double totalUptime = 0;

    public Heading(Vec2 startPosition, Vec2 endPosition) {
        this.startPosition = startPosition;
        this.endPosition = endPosition;
    }

    public void addLengthForDay(int day, double length, EventCallback callback) {
        currentAdvancements += length;
        totalUptime += SimulationConstants.TICK_INCREMENT;

        // bounds check
        while(day >= advancedMetresPerDay.size()) {
            advancedMetresPerDay.add(0.0);
        }

        while(day >= endPointsPerDay.size()) {
            endPointsPerDay.add(new Vec2(endPosition.x, endPosition.y));
        }

        Double d = advancedMetresPerDay.get(day);
        d += length;
        advancedMetresPerDay.set(day, d);
        endPosition = calculatePointFrom(length, endPosition);

        endPointsPerDay.set(day, endPosition);

        currentDrillingTime += SimulationConstants.TICK_INCREMENT;
        if (currentDrillingTime == 2 * 60 + 50) {
            currentDrillingTime = 0;
            hasJumbo = false;
            jumboDisabled = true;
            EventManager.add(new Event(this, EventType.Rehab, callback));
        }
    }

    public void addRehabMetresForNextDay(double length) {
        rehabMetresPerDay.add(length);
    }

    public double getTotalRehabLength() {
        double sum = 0;
        for(Double i : rehabMetresPerDay) {
            sum += i;
        }
        return sum;
    }

    public double distanceToHeading(Heading heading) {
        return Math.sqrt(Math.pow(endPosition.y - heading.endPosition.y, 2) + Math.pow(endPosition.x - heading.endPosition.x, 2));
    }

    public Vec2 getEndPoint(int day) {
        if(day >= endPointsPerDay.size())
            return endPosition;

        return endPointsPerDay.get(day);
    }

    /**
     * Checks if this line intersects another.
     * @param other the other line
     * @return if it intersects
     */
    public boolean intersects(Heading other) {
        return distanceToHeading(other) <= SimulationConstants.INTERSECTION_RADIUS;
    }

    /**
     * Interpolates a position on this line segment by a parameter
     * @param t the parameter
     * @return a Vec2 representing the interpolated position
     */
    public Vec2 getPositionOnLine(double t) {
        return new Vec2(lerp(startPosition.x, endPosition.y, t), lerp(startPosition.y, endPosition.y, t));
    }

    /**
     * Gets the length of this line segment.
     * @return the length
     */
    public double getLength() {
        return Math.sqrt(Math.pow(endPosition.x - startPosition.x, 2) + Math.pow(endPosition.y - startPosition.y, 2));
    }

    /**
     * Gets the angle of this line segment.
     * @return the angle
     */
    public double getAngle() {
        double x = endPosition.x - startPosition.x;
        double y = endPosition.y - startPosition.y;
        return Math.atan2(y, x);
    }

    /**
     * Linearly interpolates two values by a parameter.
     * @param v0 the first value
     * @param v1 the second value
     * @param t the parameter
     * @return the interpolated value
     */
    private double lerp(double v0, double v1, double t) {
        return (1 - t) * v0 + t * v1;
    }

    private Vec2 calculatePointFrom(double length, Vec2 v) {
        return new Vec2(v.x + length * Math.cos(getAngle()), v.y + length * Math.sin(getAngle()));
    }
}
