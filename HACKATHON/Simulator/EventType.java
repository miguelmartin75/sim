package Simulator;

public enum EventType {
    Ventilation, // = 0
    Hazard,
    ServicesReq,
    JumboDown, // after 2-3 hours
    StaffIllness,
    Flooding,
    MoveJumbo,
    Rehab
}
