//  ZDT3.java
//
//  Author:
//       Antonio J. Nebro <antonio@lcc.uma.es>
//       Juan J. Durillo <durillo@lcc.uma.es>
//
//  Copyright (c) 2011 Antonio J. Nebro, Juan J. Durillo
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

package jmetal.problems.ZDT;

import Simulator.Simulation;
import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.encodings.solutionType.*;
import jmetal.util.JMException;
import jmetal.util.wrapper.XInt;
import jmetal.util.wrapper.XReal;

/** 
 * Class representing problem ZDT3
 */
public class GASIMATTACHER extends Problem {

  private int days;
  private int jumbos;

  public GASIMATTACHER(int days, int jumbos) {
    this.days = days;
    this.jumbos = jumbos;

    numberOfVariables_  = 1;
    numberOfObjectives_ =  3;
    numberOfConstraints_=  0;
    problemName_        = "GASIMATTACHER";
        
    upperLimit_ = new double[numberOfVariables_];
    lowerLimit_ = new double[numberOfVariables_];
        
    for (int var = 0; var < numberOfVariables_; var++){
      lowerLimit_[var] = 1.0;
      upperLimit_[var] = 40.0;
    } // for
    solutionType_ = new RealSolutionType(this) ;

  } //ZDT3
      
  
  /** 
  * Evaluates a solution 
  * @param solution The solution to evaluate
   * @throws JMException 
  */    
  public void evaluate(Solution solution) throws JMException {
		//XReal x = new XReal(solution) ;
      XReal x = new XReal(solution);

    double [] f = new double[numberOfObjectives_] ;
      Simulation s = new Simulation(days,jumbos,(int)x.getValue(0),100,100, 1,false);
      Double[] fs = s.run();
    f[0] = -fs[0];
      f[1] = -fs[1];
      f[2] = -fs[2];
    solution.setObjective(0,f[0]);
    solution.setObjective(1,f[1]);
    solution.setObjective(2,f[2]);
  } //evaluate
    
  /**
   * Returns the value of the ZDT2 function G.
   * @param  x Solution
   * @throws JMException 
   */    
  private double evalG(XReal x) throws JMException {
    double g = 0.0;        
    for (int i = 1; i < x.getNumberOfDecisionVariables();i++)
      g += x.getValue(i);
    double constant = (9.0 / (numberOfVariables_-1));
    g = constant * g;
    g = g + 1.0;
    return g;
  } //evalG
   
  /**
  * Returns the value of the ZDT3 function H.
  * @param f First argument of the function H.
  * @param g Second argument of the function H.
  */
  public double evalH(double f, double g) {
    double h = 0.0;
    h = 1.0 - java.lang.Math.sqrt(f/g) 
        - (f/g)*java.lang.Math.sin(10.0*java.lang.Math.PI*f);
    return h;        
  } //evalH
} // ZDT3
